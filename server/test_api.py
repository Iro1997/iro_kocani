import unittest
import requests


class TestAPI(unittest.TestCase):
    URL ="http://127.0.0.1:5000/get"

    data={
        "name": "Test",
        "phone_num":"1234567890"
}

    def test_1_get_all_users(self):
        resp= requests.get(self.URL)
        self.assertEqual(resp.status_code,200)
        print("Test 1 Completed")

    def test_2_post_contact(self):
        resp=requests.post(self.URL,json=self.data)
        self.assertEqual(resp.status_code,200)



if __name__="__main__":
    tester= TestAPI()


tester.test_1_get_all_users()