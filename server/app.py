from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import datetime

app = Flask(__name__)
# set config
app.config['SQLALCHEMY_DATABASE_URI']= "sqlite:///database.db"


#creating database

# create db object
db=SQLAlchemy(app)

# create marshmallow object
ma= Marshmallow(app)
#create database

class Contact(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    name= db.Column(db.String(100),nullable=False)
    phone_num= db.Column(db.Integer, nullable=False)
    data_created= db.Column(db.DateTime,default = datetime.utcnow)



class ContactSchema(ma.Schema):
    class Meta:
        fields =('id','name','phone_num','data_created')


#create schemas objects

contact_schema=ContactSchema(many=False)
contacts_schema=ContactSchema(many=True)


@app.route("/contacts",methods=['POST'])
def add_contact():

    try:
        name=request.json['name']
        phone_num=request.json['phone_num']

        new_contact= Contact(name=name, phone_num=phone_num)
        db.session.add(new_contact)
        db.session.commit()


        return contact_schema.jsonify(new_contact)
    except Exception as e:
        return jsonify({
            "Error": "Invalid request"
        })

@app.route("/get",methods=['GET'])
def get_all():
    users=Contact.query.all()
    result=contacts_schema.dump(users)
    return jsonify(result)


@app.route("/get/<int:id>",methods=["GET"])
def get_one(id):
    contact=Contact.query.get_or_404(int(id))
    return contact_schema.jsonify(contact)

if __name__=='__main__':
    app.run(debug=True)